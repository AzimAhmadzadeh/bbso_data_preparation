import json

with open("2012_BBSO_filaments.json", "r") as read_file:
    data: dict = json.load(read_file)

dicts_name = [k for k in data.keys()]
info_list = data.get(dicts_name[0])
cate_list = data.get(dicts_name[1])
lice_list = data.get(dicts_name[2])
imgs_list = data.get(dicts_name[3])
anno_list = data.get(dicts_name[4])

# get all existing image ids
all_image_ids = [d.get('id') for d in imgs_list]

# pick one image id
img_id = all_image_ids[0]
image_path = ''
image_w, image_h = 0, 0
all_bbox, all_polygon = [], []

for d in imgs_list:
    if d.get('id') == img_id:
        image_path = d.get('image_path')
        image_w = d.get('width')
        image_h = d.get('height')
        break

for d in anno_list:
    if d.get('image_id') == img_id:
        all_bbox.append(d.get('bbox'))
        all_polygon.append(d.get('segmentation'))


print('For image [{}]:'.format(img_id))
print('Path: {}\nWidth: {}\nHeight: {}'.format(image_path, image_w, image_h))
print('Bbox: {}\nPolygon: {}'.format(all_bbox, all_polygon))

