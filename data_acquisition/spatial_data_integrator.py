import pandas as pd
from data_acquisition.image_explorer import ImageExplorer
from datetime import datetime, timedelta
from sunpy.net import hek
from sunpy.io import jp2  # for manipulation of jp2 images
from astropy.io import fits
import os
import csv
import re


'''
This script produces a csv file for each year of BBSO data, with the
following columns:
    TIME:
        The start time of a filament reported by HEK. This is independant
        from the existing BBSO images. In case, no matching image exists within
        a small interval around the reported time, that record will be skipped.
        Example: 2016-01-12 21:11:28
    PATH:
        The absolute path of an image in '/data/BBSO/', for which a filament is
        reported by HEK that has an start time withing a small interval of the
        image time stamp.
        Example: /data/BBSO/2016/01/12/bbso_halph_fr_20160112_211128.fts
    HAS_JPG: A boolean, True if the fts file under process has a corresponding
        jpg file, and False otherwise. This is important because it makes some
        other tasks (including 'json_generator_for_bbso') independent from
        having access to the entire bbso directory.
    IMAGE_W_ORIGINAL:
        Width of the image as the header information states.
        Example: 2048
    IMAGE_H_ORIGINAL:
        Height of the image as the header information states.
        Example: 2048
    SPATIAL_INFO:
        A list of four values extracted from the header information of the image.
        It contains respectively center_x, center_y, cdelt_x, and cdelt_y.
    BBOX:
        A list of (possibly several) bounding boxes, each corresponding to one
        filament (reported by HEK and) present in the corresponding image.
        Note: Each bounding box itself is a list of 5 points (5 lists, each of
        size 2). The points are stored in the list starting and ending at the
        bottom_left corner of the bounding box, in a counter-clockwise fashion.
    POLYGON:
        A list of (possibly several) bounding polygons, each corresponding to one
        filament (reported by HEK and) present in the corresponding image.
        Note: Each polygon itself is a list of n points (n lists, each of
        size 2). The points are stored in the list starting and ending at the
        same position, in a counter-clockwise fashion.
        
Note: The method 'integrate_data' produces a csv file with the above columns. In
this file, each row has a unique time stamp, and multiple filaments (bbox and
polygons) correspond to it. In case one filament report per row is desired, use
the method 'convert_to_one_filament_per_row' on the resultant csv files to produce
another csv file with the mentioned properties. 

'''


def integrate_data(path_to_root, image_format, time_tolerance):
    """
    Based on the images, their header information, and the filament reports
    during one year, this method generates a csv file with one row for each
    filament report with the following attributes:
    [time, path, image_width, image_height, [center_x, center_y, cdelt_x, cdelt_y], wavelength, bbox<list>, poly<list>]

    :param path_to_root: absolute path to the root directory of the images
    corresponding to one year (e.g., /data/AIA/2012/).
    :param image_format: the image format (e.g., 'jp2', 'fts', 'fits'). For
    now, it works for 'jp2' images, but it may need some minor modifications
    to be able to take in 'fts' and 'fits' formats as well.
    :param time_tolerance: the allowed tolerance (in minutes) as the differences
    between the time stamp of the queries filament and the retrieved image.
    :return: absolute path to the created csv file.
    """
    # -----------------------------------------------------
    # Create the query time interval (for one year)
    # -----------------------------------------------------
    year = re.findall(r'\d{4}', path_to_root)[-1]
    year = int(year)
    if (year < 2010) or (year > 2020):
        print('[{0}] looks like an invalid year!'.format(year))
        return

    first_month = '{0}0101_000000'.format(year)
    last_month = '{0}0101_000000'.format(year + 1)
    first_month = datetime.strptime(first_month, '%Y%m%d_%H%M%S')
    last_month = datetime.strptime(last_month, '%Y%m%d_%H%M%S')
    print('\n\tTime Interval of Interest:\t{0}--to--{1}'.format(first_month, last_month))

    # -----------------------------------------------------
    # Create a dictionary of all files: <path, timestamp>
    # -----------------------------------------------------
    print('\tReading BBSO images for the year ...'.format(year))
    fe = ImageExplorer(path_to_root)
    fe.parse_root(image_format)
    print('\n\n\tDone! Total number of images enlisted: {0}.'.format(len(fe.time_path_dict)))

    # -----------------------------------------------------
    # Generate a list of dates for beginning and ending of all 12 months
    # starting from [2012-01-01 00:00:01] to [2013-01-01 00:00:01]
    # -----------------------------------------------------
    all_months = []
    time_pivot = first_month
    while time_pivot < last_month:
        all_months.append(time_pivot)
        time_pivot = add_one_month(time_pivot)
    all_months.append(time_pivot)

    all_rows = []  # [time, path, wave, width, height, center_x, center_y, cdelt_x, cdelt_y, [bbox], [poly]]
    out_filename = ''
    n_of_instances = 0
    n_of_unique_timestamps = 0
    # -----------------------------------------------------

    # -----------------------------------------------------
    # Process data month by month
    # -----------------------------------------------------
    for i in range(len(all_months) - 1):
        file_path = ''
        print('\n\t{0}\t\tProcessing for one month starting from: {1}'
              .format(i + 1, all_months[i].strftime("%Y-%m-%d")))

        # -----------------------------------------------------
        # Query the FI instances for this period from HEK
        # For more info: http://docs.sunpy.org/en/stable/guide/acquiring_data/hek.html
        # -----------------------------------------------------
        client = hek.HEKClient()
        event_type = 'FI'       # filaments
        results = client.search(hek.attrs.Time(all_months[i], all_months[i + 1]),
                                hek.attrs.EventType(event_type))

        # -----------------------------------------------------
        # Collect all reported bboxes and polygons.
        # ---------------------------------------------------------
        #   Note: In addition to HEK's information, the header data
        #   of the JP2 images are also needed for the contours' points
        #   to be correctly converted from arcsec to pixel coordinates.
        #
        #   Note: Multiple filament reports may correspond to one
        #   single image. To avoid reading an image multiple times,
        #   we use a variable 'last_processed_date' to keep track of
        #   the time of the event. As long as the time does not change
        #   we will not try to retrieve new images.
        # -----------------------------------------------------
        all_bbox_forthistime = []
        all_poly_forthistime = []
        header_spatial_info = []
        center_x, center_y, scale_x, scale_y = 0, 0, 0, 0
        has_jpg = False
        image_w = 0
        image_h = 0

        # Initialize 'last_processed_date' in such a way that it is NOT
        # the same as any queried time ('fi_date')
        last_processed_date = datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S')
        no_image_for_this_date = False

        # -----------------------------------------------------
        # Process each filament instance.
        # Note: Below, 'res' contains one bbox and one polygon for a specific
        # date, however, there are usually multiple filaments for the same date.
        # ---------------------------------------------------------
        for res in results:
            fi_date = res['event_starttime']  # time example: 2012-01-04T18:11:55
            fi_date = datetime.strptime(fi_date, '%Y-%m-%dT%H:%M:%S')

            if fi_date != last_processed_date:
                # -----------------------------------------------------
                # If first iteration, or any iteration that the time is changed:
                #   1. Add previously collected results to 'all_rows'
                #   2. Clear the lists: all_bbox_forthistime, all_poly_forthistime
                #      since bbox and polygons should be stored in the next row
                #   3. Find the fts file corresponding to this new date.
                #   4. Get the header info, since all the results for this time is
                #      related to one fts image.
                # -----------------------------------------------------

                # if this is not the first iteration, collect the previous results
                if len(all_bbox_forthistime) != 0 or len(all_poly_forthistime) != 0:
                    print('\tCollected {0} bboxes and {1} polygons for timestamp [{2}].'
                          .format(len(all_bbox_forthistime), len(all_poly_forthistime), last_processed_date))

                    all_rows.append([last_processed_date, file_path, has_jpg, image_w, image_h, header_spatial_info,
                                     all_bbox_forthistime, all_poly_forthistime])

                    # Prepare the lists for a new timestamp
                    all_bbox_forthistime = []
                    all_poly_forthistime = []
                    header_spatial_info = []

                last_processed_date = fi_date
                no_image_for_this_date = False

                # -----------------------------------------------------
                # Find the closest image (from the image directory)
                # -----------------------------------------------------
                file_path = fe.find_closest(fi_date, time_tolerance)
                if file_path is None:
                    # No image is available for this time? Skip the rest and continue to
                    # find another timestamp
                    no_image_for_this_date = True
                    continue

                # -----------------------------------------------------
                # Check if the corresponding jpg image exists
                # -----------------------------------------------------
                image_path = '{}jpg'.format(file_path[:-3])
                has_jpg = os.path.isfile(image_path)

                # -----------------------------------------------------
                # Read the header information from fits images.
                # -----------------------------------------------------
                hdul = fits.open(file_path)
                image_w = hdul[0].header['NAXIS1']
                image_h = hdul[0].header['NAXIS2']
                center_x = hdul[0].header['CRPIX1']
                center_y = hdul[0].header['CRPIX2']
                scale_x = hdul[0].header['CDELT1']
                scale_y = hdul[0].header['CDELT2']
                header_spatial_info = [center_x, center_y, scale_x, scale_y]

            # if we already know that we don't have any image for this date, skip.
            elif no_image_for_this_date:
                continue

            # -----------------------------------------------------
            # Get the bbox/polygon points from HEK. (No conversion of units here.)
            # -----------------------------------------------------
            b = res['hpc_bbox']
            bb = cleanup_boundingpoints(b)
            all_bbox_forthistime.append(bb)

            p = res['hpc_boundcc']
            pp = cleanup_boundingpoints(p)
            all_poly_forthistime.append(pp)

            n_of_instances = n_of_instances + len(all_bbox_forthistime)

        # -----------------------------------------------------
        # END OF LOOP (res in results)
        # -----------------------------------------------------

        # All results are processed now. Add the last round to 'all_rows'
        if len(all_bbox_forthistime) != 0 or len(all_poly_forthistime) != 0:
            print('\tCollected {0} bboxes and {1} polygons for timestamp [{2}].'
                  .format(len(all_bbox_forthistime), len(all_poly_forthistime), fi_date))

            all_rows.append([fi_date, file_path, has_jpg, image_w, image_h, header_spatial_info,
                             all_bbox_forthistime, all_poly_forthistime])

        print("\tWriting (appending) the current results to a csv file...")
        # Create output file name: e.g., 2012_filaments.csv
        dir_name = os.path.basename(os.path.normpath(path_to_root))
        out_filename = ''.join([dir_name, '_filaments.csv'])

        write_to_csv(all_rows, out_filename)

        n_of_unique_timestamps = n_of_unique_timestamps + len(all_rows)

        all_rows = []

    print('\n\t-------------------------------------------------')
    print("\tIn total {0} unique time stamps were processed.".format(n_of_unique_timestamps))
    print("\tIn total {0} FI instances were collected.".format(n_of_instances))
    print("\tWriting the results to a csv file...")
    print("\tDone. Exit!")

    # -----------------------------------------------------
    # END OF LOOP (all_months)
    # All 12 months are processed.
    # -----------------------------------------------------
    return out_filename


def convert_to_one_filament_per_row(path_to_integrated_csv):
    """
    Converts the csv file created by 'integrate_data' such that each row
    corresponds to one filaments (one bbox and one polygon), instead of
    to all filements with the matching event time.

    :param path_to_integrated_csv: path to the integrated csv file created
    by 'integrate_data'.
    """
    import ast
    df = pd.read_csv(path_to_integrated_csv, sep=',')
    all_rows = []
    n_of_rows = df.shape[0]

    for i in range(n_of_rows):
        a_row = df.iloc[i]
        bbox_cell = a_row['BBOX']
        polygon_cell = a_row['POLYGON']
        all_bboxes = ast.literal_eval(bbox_cell)
        all_polygons = ast.literal_eval(polygon_cell)

        n = len(all_bboxes)
        for j in range(n):
            all_rows.append([a_row['TIME'],
                             a_row['PATH'],
                             a_row['IMAGE_W_ORIGINAL'],
                             a_row['IMAGE_H_ORIGINAL'],
                             a_row['SPATIAL_INFO'],
                             all_bboxes[j],
                             all_polygons[j]])

    out_filename = '{}_expanded.csv'.format(path_to_integrated_csv[:-4])
    write_to_csv(all_rows, out_filename)


def cleanup_boundingpoints(points_str):
    """
    :param points_str: a string of points
    :return: None if the list was empty, or a list of tuples (pairs)
    representing the bounding (box or polygon) points.
    """
    b = points_str[9:-2]
    if len(b) == 0:
        return None
    b = b.split(',')
    # trim extra spaces first, then split by space
    b = [' '.join(v.split()).split(" ") for v in b]
    return b


def add_one_month(t):
    """
    Return a `datetime.date` or `datetime.datetime` (as given) that is
    one month earlier.

    Note that the resultant day of the month might change if the following
    month has fewer days:
    """
    one_day = timedelta(days=1)
    one_month_later = t + one_day
    while one_month_later.month == t.month:  # advance to start of next month
        one_month_later += one_day
    target_month = one_month_later.month
    while one_month_later.day < t.day:  # advance to appropriate day
        one_month_later += one_day
        if one_month_later.month != target_month:  # gone too far
            one_month_later -= one_day
            break
    return one_month_later


def write_to_csv(all_rows, out_filename):
    """
    [time, path, image_width, image_height, [center_x, center_y, cdelt_x, cdelt_y], wavelength, bbox<list>, poly<list>]
    :param all_rows:
    :param out_filename:
    :return:
    """
    with open(out_filename, 'a') as csvfile:
        # [last_processed_date, file_path, image_w, image_h, header_spatial_info,
        #                                      wave, all_bbox_forthistime, all_poly_forthistime]
        fieldnames = ['TIME', 'PATH', 'HAS_JPG', 'IMAGE_W_ORIGINAL', 'IMAGE_H_ORIGINAL', 'SPATIAL_INFO', 'BBOX', 'POLYGON']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        # if the file is empty (just created) add header
        file_is_empty = os.stat(out_filename).st_size == 0
        if file_is_empty:
            writer.writeheader()
        for row in all_rows:
            writer.writerow({fieldnames[0]: row[0],
                             fieldnames[1]: row[1],
                             fieldnames[2]: row[2],
                             fieldnames[3]: row[3],
                             fieldnames[4]: row[4],
                             fieldnames[5]: row[5],
                             fieldnames[6]: row[6],
                             fieldnames[7]: row[7]})

    csvfile.close()


def main():
    import time
    # -----------------------------------------------------
    # Configuration:
    # -----------------------------------------------------
    path_to_root = '/data/BBSO/'
    file_format = 'fts'
    time_tolerance = 3  # minutes

    for i in range(2010, 2017):
        year_dir = os.path.join(path_to_root, str(i))
        out_csv_path = integrate_data(year_dir, file_format, time_tolerance)
        # time.sleep(3)
        # convert_to_one_filament_per_row(out_csv_path)


if __name__ == "__main__":
    main()
