import os
from os import walk
from datetime import datetime  # for string-to-time conversion
from sortedcontainers import SortedDict
import bisect  # for getting the closest value in SortedDict


class ImageExplorer:
    """
    This class provides two main functionalities:
        1. walking through a given directory and recursively looking
        for images with a specific image format. As a result, it creates
        a SortedDict dictionary with entries of the form:
            <key:time (type: datetime), val:path (type: str)>.
        2. finding the temporally closest item in the dictionary to any given
        time stamp, with a specified acceptable time difference.
    """

    def __init__(self, path_to_root):
        """
        Class constructor.
        :param path_to_root: absolute path to the root directory
        of the images.
        be visited.
        """
        self.f_list = []  # List of files' absolute path
        self.root = path_to_root
        self.time_path_dict = SortedDict({})  # A sorted dictionary of <key:time, val:path>

    def parse_root(self, file_format="jpg", filename_valid_signature="bbso_halph_fr"):
        """
        Walks through the root directory, and recursively visits all
        the images of the given format (i.e., 'jpg' for BBSO images)

        :param file_format: jpg, jp2, fits, or fts. Default is "jpg" for BBSO images.
        :param filename_valid_signature: a sub-string signature indicating
        which files are valid. For BBSO images, default value is "bbso_halph_fr".
        """
        import sys

        print('\n')
        for (root, dirs, filenames) in walk(self.root):

            print('\r\tCurrently visiting: [%s]' % root)
            sys.stdout.flush()

            # ----------------------------------------------------------------
            # Iterate over the files
            # ----------------------------------------------------------------
            # print('\t\tDIR: {0}'.format(dirs))
            for f in filenames:
                if (filename_valid_signature in f) and (file_format in f):
                    file_abspath = os.path.join(root, f)
                    self.f_list.append(file_abspath)

                    # Extract timestamp from the file name
                    # Example file name: f = 'bbso_halph_fr_20140611_182356.jpg'
                    if len(f) != 33:
                        continue
                    time_in_str = f[14:-4]
                    dt = datetime.strptime(time_in_str, '%Y%m%d_%H%M%S')
                    self.time_path_dict[dt] = file_abspath

    def find_closest(self, q_time: datetime, maxdiff_minutes: int) -> str:
        """
        For the given query time, it parses through the dictionary of timestamps
        and paths, and returns the path temporally closest to the queried time.
        :param q_time: datetime for which the file path should be queried.
        :param maxdiff_minutes: maximum difference (in minutes) between the queried
        and the retrieved timestamp.
        :return: the absolute path to the retrieved file, if the file exists, and None
        otherwise.

        Note: this code is based on the suggestions here:
            - https://stackoverflow.com/questions/8162379/python-locating-the-closest-timestamp
            - https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
        """
        # Find the closest key in the dictionary
        q_index = bisect.bisect_left(self.time_path_dict.keys(), q_time)

        if q_index >= len(self.time_path_dict):  # if q_time occurs after the last time stamp
            q_index = q_index - 1
        elif q_index == 0:
            pass
        else:
            # Now, we know that:
            #   before_q_time < q_time < after_q_time,
            # but we don't know which one is closer. Compute the time differences to
            # find out.
            before_q_time = self.time_path_dict.items()[q_index - 1][0]
            after_q_time = self.time_path_dict.items()[q_index][0]
            diff_from_before = q_time - before_q_time
            diff_from_after = after_q_time - q_time

            q_index = (q_index if diff_from_after < diff_from_before else q_index - 1)

        r_time = self.time_path_dict.items()[q_index][0]  # retrieved time

        # Check if the difference is acceptable
        time_delta = (q_time - r_time if q_time > r_time else r_time - q_time)
        # get the differences in (minutes, seconds)
        diff = divmod(time_delta.days * 86400 + time_delta.seconds, 60)

        if diff[0] > maxdiff_minutes:
            return None

        q_filepath = self.time_path_dict.items()[q_index][1]

        print('--> q_time:{0}\t\tr_time:{1}\n\t\tfile:{2}'.format(q_time,
                                                                  self.time_path_dict.items()[q_index][0],
                                                                  q_filepath))
        return q_filepath
