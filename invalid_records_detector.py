import os
import pandas as pd


def invalid_records_detector(path_to_spatial_csv):
    df = pd.read_csv(path_to_spatial_csv, sep=',')

    n_rows = df.shape[0]

    invalid_counter = 0
    total = 0

    for i in range(n_rows):
        a_row: pd.core.series.Series = df.iloc[i]
        path = a_row['PATH']
        path = '{}jpg'.format(path[:-3])
        total = total + 1
        exists = os.path.isfile(path)
        if exists:
            continue
        else:
            invalid_counter = invalid_counter + 1
    return invalid_counter, total


def main():

    # paths specific to files on the server.
    path_to_spatial_csv = '/home/azim1/bbso_data_preparation/2016_filaments.csv'
    invalid, total = invalid_records_detector(path_to_spatial_csv)
    print('Invalid: {}\tTotal: {}\tRatio: {}'.format(invalid, total, (invalid * 1.0) / total))


if __name__ == "__main__":
    main()
