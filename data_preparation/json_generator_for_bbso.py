from datetime import datetime

"""

This script converts the csv file created by 'spatial_data_integrator'
to a json file. To see the format of the output json file, look at
the example file 'EXAMPLE_2016_BBSO_filaments.json' which contains
only three images and their spatial information.

NOTE 1: The input csv file should have one image (i.e., one unique path)
in each row (not one filament report per row).

NOTE 2: The conversion of units (from arcsec to pixels) takes place in
this script. So, the csv file contains the unconverted data.

NOTE A: A bbox is represented by 5 points defining the four corners of the
bbox, starting and ending at the bottom-left point, hence 5 points. The points
in the list are ordered in the counter clock-wise fashion. Given that the origin
is considered the top-left corner of the canvas, the following mappings hold:

    (x_min, y_min) = bbox[3]
    (x_max, y_max) = bbox[1]

We use the coordinate of the top-left point (bbox[3]) to follow the COCO-style
format in our json file. For each bbox, the list will have the following form:
    [top_left_X, top_left_Y, image_width, image_height]
"""


def generate_json_for_bbso(image_size_tobeused, path_to_spatial_csv, path_to_ref_images, path_to_json_output):
    """
    :param image_size_tobeused: width (or height) of the images which will be used
    as the final product. If the goal is to work on downsampled images, this argument
    holds the width (or height) of the image size after downsampling.
    :param path_to_spatial_csv: path to the csv file that is going to be converted
    to json.
    :param path_to_ref_images: path to the root directory of the downsampled images.
    Note that the directory structure of the downsampled images is flat (i.e., all
    images under one directory representing one year).
    :param path_to_json_output: path (with file name) of the output json file.
    :return:
    """
    import ast
    import os
    import pandas as pd
    import json

    # ----------------------------------------------
    # Generate an annotation id for each annotation element
    # in the annotation dictionary (i.e., for each bbox & segmentation
    # pair). To be able to generate an id that remains unique across
    # different partitions (years) of data, I use year * 10000 as the
    # first id for the annotations in each year.
    # ----------------------------------------------
    year = int(os.path.basename(path_to_spatial_csv)[:4])  # extract the year from the argument
    annotation_unique_id = year * 10000

    # ----------------------------------------------
    # Read the csv file and generate the dictionaries
    # ----------------------------------------------
    df_spatial = pd.read_csv(path_to_spatial_csv, sep=',')
    annotations_list = list()
    image_dict_list = list()
    n_rows = df_spatial.shape[0]

    print('Parsing the csv file, row by row ...')
    total_images_skipped = 0

    for i in range(n_rows):

        a_row: pd.core.series.Series = df_spatial.iloc[i]

        # ----------------------------------------------
        # Convert timestamp of this image to datatime object
        # ----------------------------------------------
        filament_time = datetime.strptime(a_row['TIME'], '%Y-%m-%d %H:%M:%S')

        # ----------------------------------------------
        # Get the image path and modify it:
        #   1. fts --> jpg
        #   2. check if the corresponding jpg image exists. (For each
        #   year, there are 1 to 5 instances (out of ~200 images) whose
        #   jpg and fts timestamps do not exactly match. Since we use the
        #   header info of the fts files, the jpg time stamp should exactly
        #   match. Our solution is to drop them.
        #   3. change the parent dir to 'path_to_ref_images'
        # ----------------------------------------------
        has_jpg = a_row['HAS_JPG']   # type: numpy.bool
        if not has_jpg:
            total_images_skipped = total_images_skipped + 1
            continue

        image_path = a_row['PATH']
        image_path = '{}jpg'.format(image_path[:-3])

        image_name = os.path.basename(os.path.normpath(image_path))
        image_path = os.path.join(path_to_ref_images, image_name)

        # ----------------------------------------------
        # Generate an id for each image:
        # This is generated based on the timestamp encoded in the image
        # name.
        # Exp: 'bbso_halph_fr_20120103_182722.jpg' --> 20120103182722
        # ----------------------------------------------
        image_id = os.path.basename(os.path.normpath(image_path))[-19:-4]
        image_id = int(image_id.replace("_", ""))  # type: int

        # ----------------------------------------------
        # Get the list of all boundary boxes ( and polygons) in 'BBOX'
        # ( and 'POLYGON') column
        # ----------------------------------------------
        bbox_cell = a_row['BBOX']
        all_bboxes = ast.literal_eval(bbox_cell)  # convert str to list
        polygon_cell = a_row['POLYGON']
        all_polygons = ast.literal_eval(polygon_cell)  # convert str to list

        # ----------------------------------------------
        # Get the header info from 'SPATIAL_INFO' column
        # ----------------------------------------------
        center_x, center_y, scale_x, scale_y = ast.literal_eval(a_row['SPATIAL_INFO'])

        # ----------------------------------------------
        # Get the shrinkage ratio
        # ----------------------------------------------
        image_original_size = int(a_row['IMAGE_W_ORIGINAL'])
        shrinkage_ratio = image_original_size / image_size_tobeused

        # ----------------------------------------------
        # Create a dictionary for this image
        # ----------------------------------------------
        image_dict = {"id": image_id,
                      "date_captured": str(filament_time),
                      "image_path": image_path,
                      "file_name": os.path.basename(os.path.normpath(image_path)),
                      "width": image_size_tobeused,
                      "height": image_size_tobeused,
                      "license": 1}
        image_dict_list.append(image_dict)

        # ----------------------------------------------
        # Work on one bbox (polygon) at a time.
        # Note: The total number of bboxes is equal to the total number
        # of polygons, but in some cases, a list of n polygons may be
        # a list of n None values, if the polygons are not available.
        # ----------------------------------------------
        for a_bbox, a_polygon in zip(all_bboxes, all_polygons):
            # ----------------------------------------------
            # Convert point units (arcsec --> pixel)
            # ----------------------------------------------
            a_bbox = convert_boundingpoints_to_pixelunit(a_bbox, scale_x, scale_y, center_x, center_y,
                                                         image_original_size, shrinkage_ratio)
            a_polygon = convert_boundingpoints_to_pixelunit(a_polygon, scale_x, scale_y, center_x, center_y,
                                                            image_original_size, shrinkage_ratio)

            # ----------------------------------------------
            # Reformat bbox representation from a list of 5 points to a list as
            # follows: [top_left_X, top_left_Y, width, height]
            # (see NOTE A in the docstring of this script)
            # ----------------------------------------------
            if not a_bbox:
                a_bbox = []
            else:
                top_left_x, top_left_y = a_bbox[3][0], a_bbox[3][1]
                bottom_right_x, bottom_right_y = a_bbox[1][0], a_bbox[1][1]
                bbox_w = bottom_right_x - top_left_x
                bbox_h = bottom_right_y - top_left_y
                a_bbox = [top_left_x, top_left_y, bbox_w, bbox_h]

            # ----------------------------------------------
            # Reformat polygon representation:
            # Convert [[x,y],[x,y], ... ] to [x,y,x,y, ... ]
            # ----------------------------------------------
            if not a_polygon:
                a_polygon = []
            else:
                a_polygon = [x_y for coordinate in a_polygon for x_y in coordinate]

            # ----------------------------------------------
            # Create a dictionary for this instances' annotation
            # ----------------------------------------------
            annotation_dict = {"id": annotation_unique_id,
                               "category_id": 1,  # 1 represents filaments
                               "image_id": image_id,
                               "iscrowd": 0,
                               "area": 0,
                               "bbox": a_bbox,
                               "segmentation": [a_polygon]}

            # increment annotation id
            annotation_unique_id = annotation_unique_id + 1

            annotations_list.append(annotation_dict)

        # ----------------------------------------------
        # END OF LOOP: ALL DATA FOR ONE IMAGE IS COLLECTED.
        # Combined the results!
        # ----------------------------------------------

    # ----------------------------------------------
    # Generate dictionary of meta data for this dataset
    # ----------------------------------------------
    info_dict, categories_dict, licenses_dict = generate_meta_dictionaries()

    # ----------------------------------------------
    # Create a dictionary of all 'image_dict's generated before.
    # ----------------------------------------------
    images_dict = {"images": image_dict_list}

    # ----------------------------------------------
    # Create a dictionary of all annotation_dicts generated before.
    # ----------------------------------------------
    annotations_dict = {"annotations": annotations_list}

    # ----------------------------------------------
    # Combine all dictionaries into one single dictionary,
    # with the following keys:
    # "info", "categories", "licenses", "images", "annotations"
    # ----------------------------------------------
    main_dict = dict(info_dict, **categories_dict)
    main_dict.update(licenses_dict)
    main_dict.update(images_dict)
    main_dict.update(annotations_dict)

    # ----------------------------------------------
    # Save main_dict as a json file, with proper indentation.
    # Note: Create dir(s) if do not exist already.
    # ----------------------------------------------
    json_parent_dir = os.path.dirname(path_to_json_output)
    if not os.path.exists(json_parent_dir):
        os.makedirs(json_parent_dir)

    with open(path_to_json_output, 'w+') as fp:
        json.dump(main_dict, fp, indent=4)


def convert_boundingpoints_to_pixelunit(points, cdelt1, cdelt2, crpix1, crpix2, original_w, shrinkage_ratio):
    """
    This method converts the points coordinates from arc-sec unit to pixel unit, and meanwhile
    make 2 modifications:
        1. Vertical mirror of the points (this is required if JPG format is being used)
        2. Shrinkage of points (required if downsized images are being used.)

    :param points: a list of points
    :param cdelt1: fits/jp2 header information to scale in x direction
    :param cdelt2: fits/jp2 header information to scale in y direction
    :param crpix1: fits/jp2 header information to shift in x direction
    :param crpix2: fits/jp2 header information to shift in y direction
    :param original_w: the width of the original images. It is assumed that the images
    are in square shape, hence width and height are equal.
    :param shrinkage_ratio: a float point that indicates the ratio between
    the original image width (=height) and the downsized image width(=height),
    i.e., shrinkage_ratio = original_w / new_size. For example, for 512 X 512-image,
    shrinkage_ratio would be 2048/512 = 4.0.
    :return: A list of pairs (each of type list) representing the bounding
    (box or polygon) points, and None if the list is empty.
    """
    if not points:
        return None
    b = [(float(v[0]) / cdelt1 + crpix1, float(v[1]) / cdelt2 + crpix2) for v in points]

    # Shrink and then mirror vertically
    b = [(v[0] / shrinkage_ratio, (original_w - v[1]) / shrinkage_ratio) for v in b]

    return b


def generate_meta_dictionaries():
    """
    Generates three dictionaries of metadata, namely, "info", "licenses", and "categories".
    :return: the three generated dictionaries in the same order as listed above.
    """
    current_date = datetime.today()

    info_dict = {"info": {"contributor": "DMLab",
                          "date_created": "{}-{}-{}".format(current_date.year, current_date.month, current_date.day),
                          "description": "Filament Dataset (BBSO)",
                          "status": "Not publicly available, yet.",
                          "author": "Azim Ahmadzadeh",
                          "author_email": "aahmadzadeh1@cs.gsu.edu",
                          "author_webpage": "https://grid.cs.gsu.edu/~aahmadzadeh1/",
                          "Lab": "http://dmlab.cs.gsu.edu/",
                          "version": "0.1",
                          "year": current_date.year}}

    categories_dict = {"categories": [{"id": 1,
                                       "name": "filament",
                                       "supercategory": "solar-event"}]}

    licenses_dict = {"licenses": [{"id": 1,
                                   "name": "GPL License",
                                   "url": "https://www.gnu.org/licenses/gpl.html"}]}

    return info_dict, categories_dict, licenses_dict


def main():
    downsized_width = 512  # or 2048

    for y in range(2010, 2017):
        # A csv file based on which the json file will be generated
        # path_to_spatial_csv = '/data/BBSO_meta/bbso_original_fts_csv/{}_filaments.csv'.format(y)
        path_to_spatial_csv = '/home/aahmadzadeh1/bbso_original_fts_csv/{}_filaments.csv'.format(y)
        # The directory where the BBSO images are copied (and perhaps downsampled)
        # path_to_ref_images = '/data/bbso_coco_{}/images_{}/'.format(downsized_width, y)
        path_to_ref_images = '/home/aahmadzadeh1/bbso_coco_{}/images_{}/'.format(downsized_width, y)
        # The output json file
        # path_to_json_output = '/data/BBSO_meta/bbso_{}px_json/{}_BBSO_filaments.json'.format(downsized_width, y)
        path_to_json_output = '/home/aahmadzadeh1/bbso_coco_{}/annotations/{}_BBSO_filaments.json'.format(downsized_width, y)

        generate_json_for_bbso(downsized_width, path_to_spatial_csv, path_to_ref_images, path_to_json_output)


if __name__ == "__main__":
    main()
