import pandas as pd
import ast

"""
This script counts the number of bboxes and polygons retrieved
for all filaments in each year. In addition, it also counts
the number of hek reports the the images temporally matching
those reports. All these pieces of info are computed based on
the meta data files that are already calculated by the script
'spatial_data_integrator.py'. A sample of this collection is
stored in this project under the name '2012_filaments.csv'.
"""


# ['TIME', 'PATH', 'HAS_JPG', 'IMAGE_W_ORIGINAL', 'IMAGE_H_ORIGINAL', 'SPATIAL_INFO', 'BBOX', 'POLYGON']
path_to_2010 = '/home/azim/BBSO/bbso_original_fts_csv/2010_filaments.csv'
path_to_2011 = '/home/azim/BBSO/bbso_original_fts_csv/2011_filaments.csv'
path_to_2012 = '/home/azim/BBSO/bbso_original_fts_csv/2012_filaments.csv'
path_to_2013 = '/home/azim/BBSO/bbso_original_fts_csv/2013_filaments.csv'
path_to_2014 = '/home/azim/BBSO/bbso_original_fts_csv/2014_filaments.csv'
path_to_2015 = '/home/azim/BBSO/bbso_original_fts_csv/2015_filaments.csv'
path_to_2016 = '/home/azim/BBSO/bbso_original_fts_csv/2016_filaments.csv'

all_paths = [path_to_2010, path_to_2011, path_to_2012, path_to_2013, path_to_2014, path_to_2015, path_to_2016]


def get_number_of_jpgs(df: pd.DataFrame) -> int:
    all_jpgs: pd.Series = df['HAS_JPG']
    return sum(all_jpgs)


def get_number_of_reports(df: pd.DataFrame) -> int:
    return df.shape[0]


def get_number_of_bboxes(df: pd.DataFrame) -> int:
    all_bboxes = df['BBOX']
    lens = [1 if b is not None else 0 for bbox_list in all_bboxes for b in ast.literal_eval(bbox_list)]
    return sum(lens)


def get_number_of_polygons(df: pd.DataFrame) -> int:
    all_polys = df['POLYGON']
    poly_counter = [1 if p is not None else 0 for poly_list in all_polys for p in ast.literal_eval(poly_list)]
    return sum(poly_counter)


def main():
    import os
    stats_df = pd.DataFrame(columns=['YEAR', 'N_REPORT', 'N_JPG', 'N_BBOX', 'N_POLY'])
    idx = 0
    for path in all_paths:
        year = os.path.basename(path)[:4]
        df = pd.read_csv(path, sep=',')
        print('Calculating stats from [{}] ...'.format(os.path.basename(path)))
        stats_df.loc[idx] = [year,
                             get_number_of_reports(df),
                             get_number_of_jpgs(df),
                             get_number_of_bboxes(df),
                             get_number_of_polygons(df)]
        idx = idx + 1

    stats_df.to_csv('../meta_data_analysis.csv', sep=',')


if __name__ == "__main__":
    main()
