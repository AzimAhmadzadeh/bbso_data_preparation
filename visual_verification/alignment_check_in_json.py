import json
from PIL import Image, ImageDraw

"""
This script was used only to visually check if the bounding
boxes and polygons perfectly align with the filaments.
It requires access to the downsampled images on the server.
"""


def unpack_one_instance(path_to_json, index):
    """
    extracts the required information for an image from the json file.
    :param path_to_json: path to the json file representing the filaments
    in one year.
    :param index: an integer indicating which image's information should
    be unpacked. 0 stands for the first image, 1 for the second, and so on.
    :return: the following objects: img_id, image_path, image_w, all_bbox,
    and all_polygon
    """

    with open(path_to_json, "r") as read_file:
        data: dict = json.load(read_file)

    dicts_name = [k for k in data.keys()]
    info_list = data.get(dicts_name[0])
    cate_list = data.get(dicts_name[1])
    lice_list = data.get(dicts_name[2])
    imgs_list = data.get(dicts_name[3])
    anno_list = data.get(dicts_name[4])

    # get all existing image ids
    all_image_ids = [d.get('id') for d in imgs_list]

    # pick one image id
    img_id = all_image_ids[index]
    image_path = ''
    image_w, image_h = 0, 0
    all_bbox, all_polygon = [], []

    for d in imgs_list:
        if d.get('id') == img_id:
            image_path = d.get('image_path')
            image_w = d.get('width')
            image_h = d.get('height')
            break

    for d in anno_list:
        if d.get('image_id') == img_id:
            all_bbox.append(d.get('bbox'))
            all_polygon.append(d.get('segmentation'))

    return img_id, image_path, image_w, all_bbox, all_polygon


def draw_filaments(img_id, image_path, image_w, all_bbox, all_polygon):
    """
    draws bounding boxed and polygons of each filament present in the
    given image, on the image and saves the result as a PNG image.

    :param img_id: image id extracted from the json file. This is only
    used to properly name the output image. The name starts with the
    image id which is the timestamp of the image.
    :param image_path: absolute path to the downsampled image, extracted
    from the json file.
    :param image_w: width of the image extracted form the json file.
    :param all_bbox: a list of all bboxes representing the filaments present
    in this image.
    :param all_polygon: a list of all polygons representing the filaments
    present in this image.
    """
    im = Image.open(image_path)
    im = im.convert('RGB')
    img_draw = ImageDraw.Draw(im)

    for i in range(len(all_bbox)):
        a_bbox = all_bbox[i]
        x0, y0 = a_bbox[0], a_bbox[1]
        x1, y1 = x0 + a_bbox[2], y0 + a_bbox[3]
        img_draw.rectangle([x0, y0, x1, y1], outline="blue", width=3)
        img_draw.ellipse([x0 - 2, y0 - 2, x0 + 2, y0 + 2])
        img_draw.ellipse([x1 - 2, y1 - 2, x1 + 2, y1 + 2])
    for i in range(len(all_polygon)):
        a_poly = all_polygon[i][0]
        # convert [x, y, x, y, ...] to [(x, y), (x, y), ...]
        xy_poly = [(x, y) for x, y in zip(a_poly[0::2], a_poly[1::2])]
        img_draw.line(xy_poly, fill="red", width=3)
        n_of_points = len(xy_poly)
        # for p in range(n_of_points-1)[::2]:   # skip every other elements
        #     img_draw.ellipse((xy_poly[p] - 2, xy_poly[p+1] - 2, xy_poly[p] + 2, xy_poly[p+1] + 2), fill="red")
    im.save('__{}_filements_bbox.png'.format(img_id))


def main():
    import os
    mirror = '/home/azim/BBSO/images_2012'
    indices = [0, 10, 50, 100, 150, 200]  # index of images (between 0 and 238 for 2012)
    path_to_json = '/home/azim1/bbso_data_preparation/2012_BBSO_filaments.json'
    path_to_json = '/home/azim/CODES/PyWorkspace/bbso_data_preparation/jsons/2012_BBSO_filaments.json'
    for ind in indices:
        img_id, image_path, image_w, all_bbox, all_polygon = unpack_one_instance(path_to_json, ind)
        image_path = os.path.join(mirror, os.path.basename(image_path))

        draw_filaments(img_id, image_path, image_w, all_bbox, all_polygon)


if __name__ == "__main__":
    main()